<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
    return view('register');
    }

    public function welcome(request $request){
        $name = $request->awal;

        return view('welcome', compact('name'));
    }
}
