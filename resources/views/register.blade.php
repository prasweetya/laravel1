<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <h1>Buat Account Baru!</h1>
</head>
    <body>
    <h3>Sign Up Form</h3>
        <form action="/welcome" method="POST">
            @csrf
            <label>First name:</label><br><br>
            <input type="text" name="awal"><br><br>

            <label>Last name:</label><br><br>
            <input type="text" name="akhir"><br><br>
            
            <label>Gender:</label><br><br>
            <input type="radio" name="gend">Male<br>
            <input type="radio" name="gend">Female<br>
            <input type="radio" name="gend">Other<br><br>
            
            <label>Nationality:</label><br><br>
            <select name="nation">
                <option value="indo">Indonesian</option>
                <option value="singa">Singaporean</option>
                <option value="malay">Malaysian</option>
                <option value="auss">Australian</option>
            </select><br><br>

            <label>Language Spoken:</label><br><br>
            <input type="checkbox"> Bahasa Indonesia<br>
            <input type="checkbox"> English<br>
            <input type="checkbox"> Other<br><br>

            <label>Bio:</label><br><br>
            <textarea name="bio" cols="30" rows="10"></textarea><br>

            <input type="submit" value="Sign up">

        </form>
    </body>
</html>
